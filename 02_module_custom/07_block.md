### Documentation

Block API : [https://www.drupal.org/docs/8/api/block-api](https://www.drupal.org/docs/8/api/block-api)

### Dans le module examples

Regarder le modules **block_example**.

`drush en -y block_example`

--------------------------------------------------------------------------------

### Construire un formulaire

Créer CustomBlock.php dans le dossier custom_module/src/Plugin/Block.
Vous pourrez ensuite le positionner dans le layout de block.

```
<?php

namespace Drupal\module_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *  id = "block_simple",
 *  admin_label = @Translation("Block simple"),
 * )
 */
class CustomBlock extends BlockBase {

  public function build() {
    $foo = 'foo';
    $bar = 'bar';

    return [
      '#markup' => '<p>'.$foo.'</p><p>'.$bar.'</p>',
    ];
  }
}
```

--------------------------------------------------------------------------------

### Ce que l'on peut faire avec les blocks

* Créer des formulaires de configuration [https://www.drupal.org/docs/8/api/block-api/block-api-overview#s-add-custom-configuration-options-to-your-block](https://www.drupal.org/docs/8/api/block-api/block-api-overview#s-add-custom-configuration-options-to-your-block) ;
* Créer des template spécifique grâce au hook_theme();
