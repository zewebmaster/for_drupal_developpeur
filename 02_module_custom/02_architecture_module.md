### Documentation

Créer son propre module : [https://www.drupal.org/docs/8/creating-custom-modules](https://www.drupal.org/docs/8/creating-custom-modules)

### Déclarer un module

Seul fichier nécessaire : custom_module.info.yml

```
core: 8.x    
type: module
name: 'Custom module'
package: 'Custom'
description: ''
libraries:
```

### Autres fichiers de configuration

Fichier | Fonction
------- | --------
custom_module.module | Déclaration des hooks
custom_module.services.yml | Déclaration des services
custom_module.routing.yml | Déclaration des routes
custom_module.links.menu.yml | Déclaration des liens de menu
custom_module.permissions.yml | Déclaration des permissions
custom_module.librairies.yml | Déclaration des librairies utlisées
custom_module.install | Déclaration des traitements à réaliser à l'installation du module

**Bonne pratique**
Vous pouvez implémenter un hook_help() qui donnera des informations sur le propos du module/

```
<?php

use \Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function custom_module_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.custom_module':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('My module is able to do wonderfull stuff') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('Just install.') . '</p>';
      return $output;  }
}
```

### Les sources de votre application

Dans un dossier src, vous déclarez les composants de votre application.
C'est une architecture qui repose sur la philosophie symfony.

```
/src
/src/Controller
/src/Form
/src/Plugin
/src/Plugin/Block
/src/Service
```

### Des exemples d'implémentation

Nous utiliserons pour cela le module **examples**, module communautaire, qui a pour vocation à montrer les bonnes méthodes d'implémentations.

Pour installer le module examples

`composer require drupal/examples`

Il est composé d'une série de modules individuels et testables qui font le focus sur une fonctionnalité.
L’exploration du contenu du module vous éclairera sur ce que vous pourrez découvrir.
