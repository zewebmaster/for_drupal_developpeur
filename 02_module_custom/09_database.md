### Documentation

Database API : [https://www.drupal.org/docs/8/api/database-api](https://www.drupal.org/docs/8/api/database-api)

--------------------------------------------------------------------------------

### Pour faire une requête sur les tables

Requête dynamique : [https://www.drupal.org/docs/8/api/database-api/dynamic-queries/introduction-to-dynamic-queries](https://www.drupal.org/docs/8/api/database-api/dynamic-queries/introduction-to-dynamic-queries)

Un exemple de select

```
<?php

$connection = \Drupal::database();
$query = $connection->select('node', 'n')
  ->condition('n.type', 'bundle_type', '=')
  ->condition('n.nid', 1, '>')
  ->fields('n', ['nid']);
$result = $query->execute();
```

Autres exemples de requête faites sur les tables :

* update : [https://www.drupal.org/docs/8/api/database-api/update-queries](https://www.drupal.org/docs/8/api/database-api/update-queries)
* insert : [https://www.drupal.org/docs/8/api/database-api/insert-queries](https://www.drupal.org/docs/8/api/database-api/insert-queries) ;
* delete : [https://www.drupal.org/docs/8/api/database-api/delete-queries](https://www.drupal.org/docs/8/api/database-api/delete-queries)

--------------------------------------------------------------------------------

### Pour faire une requête sur les entités

Un article : [https://www.sitepoint.com/drupal-8-version-entityfieldquery/](https://www.sitepoint.com/drupal-8-version-entityfieldquery/)

Un exemple de select sur une entité de type node

```
<?php

$type   = array('node');
$query  = \Drupal::entityQuery('node')
  ->condition('field_date', '2019-09-19')
  ->condition('type', 'bundle_type', '')
$nids = $query->execute();
$result = sizeof($nids);
```
