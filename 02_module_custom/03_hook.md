### Documentation

Comprendre les hooks : [https://www.drupal.org/docs/8/creating-custom-modules/understanding-hooks](https://www.drupal.org/docs/8/creating-custom-modules/understanding-hooks)

--------------------------------------------------------------------------------

### Altérer le comportement natif des modules

C'est un concept historique de Drupal.

Les hooks répondent à des déclencheurs et permettent d'altérer le comportement du système en intervenant dans le workflow du traitement des données.

Parmi les hooks les plus utilisés, le hook_form.

```
<?php

/**
 * Implements hook_form_alter()
 */
function custom_module_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  dump($form_id);
}
```

En général, les modules implémentant des hooks vous proposent une API sur la manière des les implémenter.

Un exemple dans le module views : /core/modules/views/views.api.php

Ils tendent à disparaître avec Drupal 8, remplacés par d'autres mécanismes d'interaction (les Plugins, les Events, configuration yml).

[Liste des hooks](https://api.drupal.org/api/drupal/core!core.api.php/group/hooks/8)
