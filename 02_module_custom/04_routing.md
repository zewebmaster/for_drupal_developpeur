### Documentation

Docs : [https://www.drupal.org/docs/8/api/routing-system](https://www.drupal.org/docs/8/api/routing-system)

--------------------------------------------------------------------------------

### Système de routing

Définir une route, c'est définir un patron de chemin, prenant ou non des arguments, qui permet d'appeler le controller devant réaliser l'action.

Un path est un chemin défini d'une route.

**noter**

Créer une route ne vous créer pas automatiquement un lien dans un menu. Pour créer les liens de menus pointant vers vos controller, il vous faudra les déclarer, en back-office ou dans le fichier de configuration de votre module.

--------------------------------------------------------------------------------

### Les propriétés d'une route

Déclaration des routes dans le fichier `custom_module.routing.yml`

```
custom_module.route_name:
  path: (required) '/examples/arguments/{foo}/{bar}'
  defaults: (required)
    _controller: \Drupal\[custom_module]\Controller\[ClassName]::[method]
  requirements (required):
    _permission:
    _role:
    foo: \d+
    bar: \d+
```

Tous les paramètres possibles : [https://www.drupal.org/node/2092643](https://www.drupal.org/node/2092643)

**À savoir**

* Les arguments de route et les arguments du controller doivent être déclarés dans le même ordre, avec la même nomenclature ;
* Les arguments sont nommés `{node}` ou `{user}` et peuvent être chargés dans le Controller en les typant des bonnes classes ;

```
path: (required) '/examples/load-node/{node}'
public function build(\Drupal\node\NodeInterface $node) {}

path: (required) '/examples/load-user/{user}'
public function build(\Drupal\Core\Session\AccountInterface $user) {}
```

* On peut gérer de manière dynamique en faisant référence à des fonctions callback, en déclarant dans les `requirements` la valeur `_custom_access: '\Drupal\[custom_module]\Controller\[CheckAccessCustomController]::[checkAccessForRoute]'` (doit renvoyer `Drupal\Core\Access\AccessResult::allowed()` ou `Drupal\Core\Access\AccessResult::forbidden()` )

--------------------------------------------------------------------------------

### Configuration yml

Vous pouvez déclarer plusieurs types de fichiers de configuration selon le niveau de menu :

File | Configuration
---- | -------------
custom_module.links.menu.yml | lien de menu dans l'arborescence
custom_module.links.task.yml | onglet
custom_module.links.action.yml | "action" (back-office)

Un exemple simple de fichier de configuration de menu

```
custom_module.page_simple:
  menu_name: main
  title: 'Page simple'
  route_name: module_core.test_controller
  weight: 1
```
--------------------------------------------------------------------------------

### Dans le module examples

On pourra regarder des examples simples de routes et de controller dans le module **page_example**.

`drush en -y page_example`
