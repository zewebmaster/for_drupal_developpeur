### Documentation

Form API : [https://www.drupal.org/docs/8/api/form-api](https://www.drupal.org/docs/8/api/form-api)

### Dans le module examples

Regarder le modules **form_api_example**.

`drush en -y form_api_example`

--------------------------------------------------------------------------------

### Construire un formulaire

Créer CustomForm.php dans le dossier custom_module/src/Form/ et déclarer une route dans votre fichier de déclaration .routing.yml

```
<?php

namespace Drupal\custom_module\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CustomForm extends FormBase {

  public function getFormId() {
   return 'custom_form';
  }  
  public function buildForm(array $form, FormStateInterface $form_state) {
    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}
```

```
custom_module.example_form:
  path: '/path/to/my/custom-form'
  defaults:
    _form:  '\Drupal\custom_module\Form\CustomForm'
    _title: 'Custom Form'
  requirements:
    _permission: 'access content'
```

En savoir plus : [https://www.drupal.org/docs/8/api/form-api/introduction-to-form-api](https://www.drupal.org/docs/8/api/form-api/introduction-to-form-api)

**À noter** Vous pouvez hooker votre propre formulaire.

--------------------------------------------------------------------------------

### Les méthodes plus en détails

Méthode | fonctionnalité
------- | --------------
getFormId | Id du formulaire
buildForm | Construction du formulaire
validateForm | Vérification custom des entrées du formulaire
submitForm | Traitement du formulaire

--------------------------------------------------------------------------------

### Les différents inputs

Regarder dans le module form_api_example, le fomulaire de démonstration.

* source : `src/Form/InputDemo.php` ;
* path : `/examples/form-api-example/input-demo` ;

Vous pouvez également regarder l'api de référence des formulaires : [https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x](https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x).

--------------------------------------------------------------------------------

### Pour aller plus loin dans la gestion des formulaires

#### State API

C'est une API qui permet de conditionner l'affichage de certain champs en fonction de sélection faire sur d'autre champs.
Vous trouverez des examples d'implémentation Ajax dans le module **form_api_example : StateDemo.php**

#### Utiliser Ajax

Vous trouverez des examples d'implémentation Ajax dans le module **ajax_example** et le module **form_api_example**
