### Éléments de correction

#### Implementation du hook :

- [ ] J'ai bien nommé mon hook custom_module_form_alter() ;

```
/**
 * Implements hook_form_alter()
 */
function custom_module_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  if ($form_id == 'node_article_form') {
    $form['title']['#access'] = false;
    $form['title']['widget'][0]['#disabled'] = TRUE;
    $form['title']['widget'][0]['value']['#default_value'] = 'Mon titre custom';
  }
}
```
##### L'utilisateur courant

Faire un kint() sur l'utilisateur courant vous permettra de vous familiariser avec les méthodes que vous pouvez utiliser sur l'entité et les classes nécessaires pour manipuler les données.

```
<?php

/**
 * Implements hook_form_alter()
 */
function custom_module_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  if ($form_id == 'node_article_form') {

    $user = \Drupal::currentUser();
    $username = $user->getUsername();
    $form['title']['#access'] = false;
    $form['title']['widget'][0]['value']['#default_value'] =  sprintf('Permanence de %n', $username);
  }
}
```

**À noter**

* `\Drupal::currentUser()` retourne AccountProxyInterface ;
* `\Drupal\user\Entity\User::load()` retourne l'entité complète ;
