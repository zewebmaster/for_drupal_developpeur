### Réalisation d'un module custom

#### Consigne

Créer un module custom et implémenter un hook_form, qui modifie le formulaire de contribution d'une permanence pour :

* Faire disparaître le champs de saisie du titre ;  
* Affecter au titre une valeur par défaut ;

Vous pouvez essayer d'aller plus loin en essayant d'attraper une information sur l'utilisateur courant et la mettre dans le titre : *Permanence username*

#### Guide

* Vous pouvez utiliser un dump() ou un kint() pour inspecter la structure de la données ;
* Pour faire disparaître le champs titre (ou un autre champ), vous pouvez regarder du côté de la propriété `#access` ;
* De la même manière, pour définir une valeur par défaut, la propriété `#default_value`;
* Pour récupérer l'utilisateur courant, vous pouvez utiliser la fonction `$user = \Drupal::currentUser();`
