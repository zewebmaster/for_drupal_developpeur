### Éléments de correction

--------------------------------------------------------------------------------

#### Niveau 1

Bien injecter le service dans la construction du fomulaire

```
<?php

use Drupal\custom_module\Service\CustomService;
use Symfony\Component\DependencyInjection\ContainerInterface;

public function __construct(CustomService $custom_service) {
  $this->custom_service = $custom_service;
}

public static function create(ContainerInterface $container) {
  return new static($container->get('custom_module.custom_service'));
}
```

* Formatter la réponse du service pour qu'elle renvoie un tableau d'options

```
return array[
  'none' => 'Aucun',
  'term_id' => 'Term name'
]

```

* dans le formulaire, appeler la fonction dans le buildForm()

```
$form['pole'] = [
  'type' => 'select',
  'options' => $this->custom_service->get_term_list('pole');
]
```
--------------------------------------------------------------------------------

#### Niveau 2

Implémentation spécifique du service, car dans un plugin Block

```
<?php

namespace Drupal\custom_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\custom_module\Service\CustomService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "custom_bloc_with_service",
 *  admin_label = @Translation("Block custom avec l'utilisation d'un service"),
 * )
 */
class CustomBlockWithService extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\custom_module\Service\CustomService
   */
  protected $custom_service;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('custom_module.custom_service')
    );
  }

  public function __construct(array $configuration, $plugin_id, $plugin_definition, MonService $service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->custom_service = $custom_service;
  }
}
```
