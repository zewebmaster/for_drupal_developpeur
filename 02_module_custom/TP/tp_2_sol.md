### Éléments de correction

--------------------------------------------------------------------------------

#### Niveau 1

```
custom_module.routing.yml

module_name.permanence_form:
  path: '/permanence/creer'
  defaults:
    _form:  '\Drupal\custom_module\Form\CustomForm'
    _title: 'Custom Form'
  requirements:
    _permission: 'access content'
```

```
custom_module.links.menu.yml

module_name.custom_form:
  menu_name: footer
  title: 'Saisir une permanence'
  route_name: module_name.permanence_form
  weight: 1
```

--------------------------------------------------------------------------------

#### Niveau 2

Implémentation d'une validation custom

```
<?php

public function validateForm(array &$form, FormStateInterface $form_state) {
  // récupération de la valeur du champ
  $pole = $form_state->getValue('pole');
  // génération d'un message d'erreur
  if($pole == 'none') {
    $form_state->setErrorByName('pole', $this->t('Please select a pole'));
  }
}
```

Implémentation de l'API state
```
$form['post_comment'] = [
  '#type' => 'checkbox',
  '#title' => $this->t('Add a comment'),
];
$form['comment'] = [
  '#type' => 'texarea',
  '#title' => $this->t('Post your comment'),
  '#states' => [
    'invisible' => [
      ':input[name="post_comment"]' => ['checked' => FALSE],
    ],
  ],
];

```

--------------------------------------------------------------------------------

#### Niveau 3

Récupérer l'utilisateur courant : `$account = \Drupal::currentUser();`

Création d'un node

```
// Déclaration
// use Drupal\node\Entity\Node;

$fields = [
  'type' => 'permanence',
  'title' => 'Permanence de ' . $account->getUsername(),
  'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
  'uid' => $account->id(),
  'status' => 1,
];

$node = Node::create([
  'type' => 'permanence',
  'title' => 'Permanence de ' . $account->getUsername(),
  'langcode' => 'fr',
  'uid' => $account->id(),
  'status' => 1,
]);

$node->set('field_benevole', '');
$node->set('field_debut', '');
$node->set('field_fin', '');
$node->set('field_commentaire', '');
$node->save();
```

Implémentation de redirection

```
Dans le submitForm()
$parameters =  [
  'foo' => $foo,
  'bar' => $bar,
];
$form_state->setRedirect('custom_module.callback_form', $parameters);

// Dans le controller
public function callback_form() {
  $params  = \Drupal::request()->query->all();
  $foo =  array_key_exists('foo', $params) ? $params['foo'] : '';
  $bar  =  array_key_exists('bar', $params) ? $params['bar'] : '';

  return [
    '#markup' => sprintf('Hello %f %b', $foo, $bar);
  ];
}
```
