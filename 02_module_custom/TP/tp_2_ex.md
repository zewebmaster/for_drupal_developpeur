### Créer un formulaires et renvoyer les données vers un controller qui les affichera

#### Consigne

Créer un formulaire custom pour la saisie d'une permanence.

Niveau 1 :

* Faire le formulaire avec les 4 champs : date de début, date de fin, pôle, commentaire.
* Vous pouvez éventuellement créer un lien vers ce formulaire dans un menu footer ;

Niveau 2 :

* Implémenter une validation custom pour un champ de formulaire : *champs pôle nom vide* ... ;
* Utiliser l'api state sur une checkbox et un champs commentaire ;

Niveau 3 :

* Récupérer l'id de l'utilisateur courant et créer un node.
* Renvoyer vers une page ou un controller avec un message. 
