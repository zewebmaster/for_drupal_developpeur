### Implémentation d'un service

#### Consigne

Vous devez implémenter un service dans un composant, nouveau ou existant.

* Niveau 1 : Implémenter un service dans le formulaire pour aller rechercher dynamiquement les options du select sur les pôles à partir de la taxonomie ;
* Niveau 2 : Créer un bloc et un service pour la liste des termes d'un vocabulaire ;
* Niveau 3 : Même bloc que le niveau 2 mais rend un tableau avec une liste, par pôle du nombre de permanences.

#### Guide
x
* Soyez bien attentif à faire toutes les déclarations dans les fichiers de configuration ;
* Vous devrez commencer à manipuler l'API Term pour charger et récupérer les informations du vocabulaire ;

```
$entity_type  = 'taxonomy_term';
$vid = 'nom_machine_vocabulaire';
$terms = \Drupal::entityTypeManager()->getStorage($entity_type)->loadTree($vid);
if(!empty($terms)) {
  foreach($terms as $term) {
    kint($term);
  }
}
```
