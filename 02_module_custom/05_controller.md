### Implémenter un Controller

Créer CustomController.php dans le dossier custom_module/src/Controller/ et déclarer une route dans votre fichier de déclaration .routing.yml

```
<?php

namespace Drupal\custom_module\Controller;

use Drupal\Core\Controller\ControllerBase;

class CustomController extends ControllerBase {
  public function render() {
    $foo = 'Hello world';
    $bar = ' \o/ ';
    return [
      '#markup' => $foo . $bar,
    ];
  }
}
```
--------------------------------------------------------------------------------

### Gestion des arguments

Un controller peut prendre des arguments en paramètre. Ils doivent être en cohérence avec la déclaration de la route.

* Vous pouvez faire des vérifications sur le typage des paramètre dans la route ;
* Vous pouvez typez les arguments du Controller ;
* Vous pouvez charger directement des entités - *équivalent de l'autoload du hook_menu()* ;

--------------------------------------------------------------------------------

### La gestion du render

Un controlleur renvoie une réponse formatté dans un tableau formatté.
Vous pouvez renvoyer un simple markup, mais vous pouvez également utilisé de nombreux paramètres pour structurer le rendu de votre réponse.

Liste des paramètres : [https://www.drupal.org/docs/8/api/render-api/render-arrays#properties](https://www.drupal.org/docs/8/api/render-api/render-arrays#properties)

--------------------------------------------------------------------------------

### Dans le module examples

Regarder les modules **page_example** et **render_example**

`drush en -y page_example render_example`


### Documentation

render API : [https://www.drupal.org/docs/8/api/render-api](https://www.drupal.org/docs/8/api/render-api)
