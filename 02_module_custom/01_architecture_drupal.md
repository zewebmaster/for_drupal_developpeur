### La structure de Drupal

#### Core

C'est le dossier qui regroupe toutes les composantes du core.

Il y a un sous dossier modules, qui regroupe tous les modules composant le core de Drupal, et un dossier thème qui embarque les thèmes natifs.

#### Modules

C'est le dossier dans lequel on va regrouper tous les modules de notre application, qu'ils soient contributeurs ou personnels.

* un dossier contrib ;
* un dossier custom ;

**À noter :**
L'installation d'un module avec composer `composer require drupal/module_name` range automatiquement le module dans le bon dossier.
Pour vos modules custom, c'est à vous de le créer.

#### Thèmes

Identique au module, avec les même règles de bonnes pratiques.

* un dossier contrib ;
* un dossier custom ;

#### Sites

* Toutes les ressources (fichiers et média) de votre site ;
* Les fichiers de configuration ;

#### Profiles

Les profils d'installation de votre application.

*On verra plus loin la notion de profils d'installation*
