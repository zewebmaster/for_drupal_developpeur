### Principe de l'injection de container

Mécanisme hérité des composants symfony, il permet d'injecter des classes dans la construction d'un controlleur, d'un formulaire.

Pour des logiques métiers, des manipulations spécifiques de données, il est possible de définir un/des services, des classes qui vont regrouper les traitements à réaliser.

--------------------------------------------------------------------------------

### Implémenter un service

#### Déclarer un service

Dans un fichier de configuration `custom_module.services.yml`

```
services:
  custom_module.custom_service:
    class: Drupal\custom_module\Service\CustomService
    arguments: []
    tags:
```

#### Créer la classe

Dans le dossier `custom_module/src/Service/CustomService.php`

```
<?php

namespace Drupal\custom_module\Service;

class CustomService {
  public function do_foo(){
    return 'foo' ;
  }
  public function do_bar(){
    return 'bar' ;
  }
}
```

--------------------------------------------------------------------------------

### Exemple d'utilisation dans un controller

Dans le dossier `custom_module/src/Controller/CustomControllerWithService.php`

```
<?php

namespace Drupal\custom_module\Controller;

use Drupal\custom_module\Service\CustomService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomControllerWithService extends ControllerBase {

  /**
   * @var \Drupal\custom_module\Service\CustomService
   */
  protected $custom_service;

  public function __construct(CustomService $custom_service) {
    $this->custom_service = $custom_service;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('custom_module.custom_service'));
  }

  public function render() {
    $output = $this->custom_service->do_foo();
    $output .= $this->custom_service->do_bar();

    return [
      '#markup' => $output;
    ]
  }
}
```

--------------------------------------------------------------------------------

### Des services natifs de Drupal

Appeler un service dans un controller :

```
$service = \Drupal::service('service.name')
$service->method()
```

Les services à connaître :

* router.admin_context (->isAdminRoute())
* path.matcher (->isFrontPage() / ->match())
* plugin.manager.mail (->mail())
* title_resolver (->getTitle())
* entity_type.manager (->getStorage())
* ...

Ensemble des services utilisables :  [https://api.drupal.org/api/drupal/services/8.8.x](https://api.drupal.org/api/drupal/services/8.8.x)
