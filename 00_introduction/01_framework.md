### Drupal 8 est premièrement un CMS

* Drupal utilisable out of the box ;
* Permet de gérer des contenus, de les organiser (menu / agrégation de contenu), de les présenter dans un thème (bartik).

### Drupal 8 est modulaire

* Des modules pour étendre le comportement natif de Drupal ;
* Des thèmes, gratuit / payant ;
* La possibilité de développer ses propres modules & thèmes ;

### Drupal 8 est un CMF

* Repose sur les dernières normes php ;
* Beaucoup plus orientée POO ;
* Il est possible de construire des applications sur-mesure ;
