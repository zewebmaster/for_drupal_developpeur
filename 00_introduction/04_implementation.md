### Des modules pour améliorer le back office de Drupal

L'interface native de Drupal est un peu rugueuse. Elle est gérée par un thème (Seven) que je trouve peu ergonomique.

Quelques modules vont, d'une part rendre l'interface d'administration plus agréable et d'autre part, nous offrir des outils de debbuging.

* [https://www.drupal.org/project/adminimal_theme](https://www.drupal.org/project/adminimal_theme)
* [https://www.drupal.org/project/admin_toolbar](https://www.drupal.org/project/admin_toolbar)
* [https://www.drupal.org/project/adminimal_admin_toolbar](https://www.drupal.org/project/adminimal_admin_toolbar)
* [https://www.drupal.org/project/module_filter](https://www.drupal.org/project/module_filter)
* [https://www.drupal.org/project/devel](https://www.drupal.org/project/devel)

#### Installation

Dans votre projet :

```
composer require drupal/adminimal_theme
composer require drupal/adminimal_admin_toolbar
composer require drupal/admin_toolbar
composer require drupal/devel
composer require drupal/module_filter
drush then -y adminimal_theme
drush en -y adminimal_theme adminimal_admin_toolbar admin_toolbar_tools admin_toolbar_tools module_filter devel devel_generate kint
drush cr
```

--------------------------------------------------------------------------------

### Des modules très utilisés

Intégration de contenu

* [https://www.drupal.org/project/paragraphs](https://www.drupal.org/project/paragraphs) ;

Formulaire

* [https://www.drupal.org/project/address](https://www.drupal.org/project/address) ;
* [https://www.drupal.org/project/honeypot](https://www.drupal.org/project/honeypot) ;

SEO

* [https://www.drupal.org/project/pathauto](https://www.drupal.org/project/pathauto) ;
* [https://www.drupal.org/project/metatag](https://www.drupal.org/project/metatag) ;
* [https://www.drupal.org/project/sitemap](https://www.drupal.org/project/sitemap) ;

Administration

* [https://www.drupal.org/project/ultimate_cron]()
