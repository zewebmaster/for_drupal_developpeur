### Documentation

Configuration drupal : [https://www.drupal.org/docs/8/api/block-api](https://www.drupal.org/docs/8/api/block-api)

### Tutoriel

1 - Créer un fichier de configuration local à partir de l'exemple fourni et décommenter certaines lignes ;

```
cd /web/sites
cp example.settings.local.php /default/settings.local.php
cd default
vim settings.local.php

$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
```

2 - Modifier le fichier de configuration des services en mode développement pour rajouter les lignes suivantes

```
cd /web/sites
vim developpement.service.yml

parameters:
  http.response.debug_cacheability_headers: true
  twig.config:
    debug: true
    auto_reload: true
services:
  cache.backend.null:
    class: Drupal\Core\Cache\NullBackendFactory
```

3 - Ouvrir le fichier settings.php et décommenter la ligne suivante

```
cd /web/sites/default
vim settings.php

// l. 755
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
```

4 - Vider les caches

`drush cr`
