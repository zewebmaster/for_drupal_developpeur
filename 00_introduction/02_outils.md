### Les outils du développeur Web

Un bon terminal :

* Pour linux, on peut utiliser Tilix ;
* Pour windows, on peut utiliser [cmder](https://cmder.net/), un émulateur de ligne de commande linux ;

Un bon IDE avec un debugger intégré (Xdebug)

* Atom, phpStorm, VisualStudio ;

Quelques plugins utiles pour votre navigateur

* Firefox Multi-Account Containers ;
* OpenLorem ;
* Awesome Timestamp ;
* Xdebug-ext ;


--------------------------------------------------------------------------------

### Le serveur Web

Pour installer sur un ordinateur en local votre site Drupal 8.

Télécharger un utilitaire de gestion de serveurs :

* Windows : [WAMPP](http://www.wampserver.com/)
* Linux : [XAMMP](https://www.apachefriends.org/fr/index.html)
* Mac : [MAMP](https://www.mamp.info)


**Configuration requise** :

* php > 7.0 ;


--------------------------------------------------------------------------------

### Git

Git vous sera essentiel tout au long du projet, pour

* Partager votre code ;
* Versionner son projet ;
* Faciliter le déploiement du projet ;


#### Installation Linux

```
sudo apt-get install git-all
```

#### Installation Windows

Télécharger l'executable :
[http://git-scm.com/download/win](http://git-scm.com/download/win)

*À noter git est déjà implémenter avec l'utilitaire de terminal cmder.*

--------------------------------------------------------------------------------

### Composer

Composer est un utilitaire php qui permet de gérer le versionning et les dépendances des librairies d'un projet.
Le core de Drupal & les modules contributeurs seront gérés par Composer pour leur installation et leur mise à jour.

[En savoir plus sur Composer](https://getcomposer.org)

#### Installation en global sous linux :

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
// Pour installer composer globalement sur votre machine
mv composer.phar /usr/local/bin/composer
// Pour connaître les fonctions utilisables
composer
```

#### Installation sur Windows

* Télécharger l'executable :
[https://getcomposer.org/download/](https://getcomposer.org/download/) ;
* S'assurer de la version de php implémentée et télécharger une version supérieur à 7.0 si nécessaire.
[https://windows.php.net/download/](https://windows.php.net/download/)

**À noter**
On peut également utiliser la procédure linux dans l'émulateur linux. On s'arretera au téléchargement du fichier *composer.phar*.

--------------------------------------------------------------------------------

### Drush

Drush = Drupal Shell

C'est un ensemble de script qui facilite grandement l'administration de son site Drupal au quotidien.
[https://drushcommands.com/](https://drushcommands.com/)

* Pour vider les caches ;
* Pour installer / desinstaller des modules ;
* Pour gérer l'import/export des configurations
* Pour installer sur site ;
* Pour mettre à jour, exporter sa bdd ;
* ... ;

#### Installation en global :

Source :

* [http://docs.drush.org/en/master/install/](http://docs.drush.org/en/master/install/)
* [https://github.com/drush-ops/drush-launcher/](https://github.com/drush-ops/drush-launcher/)


Installer Drush globalement : pour lancer Drush dans n'importe quelle projet Drupal.

```
wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.6.0/drush.phar
chmod +x drush.phar
sudo mv drush.phar /usr/local/bin/drush
drush
```

#### Installation sur Windows

Une première méthode : installer le launcher

* Source : [https://github.com/drush-ops/drush-launcher](https://github.com/drush-ops/drush-launcher)
* Source : [http://modulesunraveled.com/drush/installing-drush-windows](http://modulesunraveled.com/drush/installing-drush-windows)

Une seconde méthode : installer drush avec composer

```
cd www/
composer require drush/drush
vendor/bin/drush
```

--------------------------------------------------------------------------------

### La console Drupal

Si vous êtes familiarisé avec la console symfony, cela peut être un plus.
