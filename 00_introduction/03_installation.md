### Configurer ses serveurs

* Allumer vos serveurs php & mysql ;
* Configurer éventuellement vos hôtes virtuels ;

Une ressource : [http://www.nicolas-verhoye.com/comment-configurer-virtual-hosts-wamp.html](http://www.nicolas-verhoye.com/comment-configurer-virtual-hosts-wamp.html)

### Créer votre base de données

* [http://localhost/phpmyadmin](http://localhost/phpmyadmin)
* name : drupal_project
* interclassement : utf8mb4_general_ci

--------------------------------------------------------------------------------

### Télécharger les sources

On téléchargera Drupal avec Composer, depuis le repo. officiel.

En savoir plus sur l'installation

[https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#download-core](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#download-core)


Dans le répétoire web de votre serveur local.

```
cd www
composer create-project --prefer-dist drupal/recommended-project drupal_project
```

On peut en profiter pour télécharger Drush.

```
composer require drush/drush
```

--------------------------------------------------------------------------------

### Installer Drupal

* En vous rendant à l'adresse déclarée dans la configuration de votre serveur virtuel.
* Avec drush `drush site-install`

**À noter**

En local, pour un bac à sable, il n'est pas nécessaire de traduire votre Drupal en français.
Vous vous familiariserez avec les nomenclatures en anglais et cela vous aidera dans vos recherches d'information sur l'API Drupal ou sur le web.

--------------------------------------------------------------------------------

### Gitter le projet

Pour commencer l'historique de votre projet et déposer votre projet dans votre github.

```
cd drupal8_sandbox
git init
git add .
git commit -m "initialisation du projet"
git remote add origin *my_repositoy_address.git*
git push -u origin master

```

**À noter**

Penser à vérifier la présence d'un .gitignore. Si il n'y a pas de .gitignore, copier l'exemple. Vous pouvez reprendre la configuration suivante :

```
# Ignore dependencies that are managed with Composer.
# Generally you should only ignore the root vendor directory. It's important
# that core/assets/vendor and any other vendor directories within contrib or
# custom module, theme, etc., are not ignored unless you purposely do so.
vendor/

# Ignore core when managing all of a project's dependencies with Composer
# including Drupal core.
web/core/
web/modules/contrib/
web/themes/contrib/
web/profiles/contrib/
web/libraries/

# Ignore configuration files that may contain sensitive information.
web/sites/*/settings*.php
web/sites/*/services*.yml

# Ignore paths that contain user-generated content.
web/sites/*/files/
web/sites/*/private/

# Ignore SimpleTest multi-site environment.
web/sites/simpletest
```
