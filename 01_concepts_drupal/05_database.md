### Configuration

La configuration de la connexion à la base de données se trouve dans le fichier de settings.

`sites/default/settings.php`

Dans ce fichier, vous pouvez également déclarer d'autres bases de données auxquelles vous voudriez vous connecter.

*On verra plus loin la manipulation de la base de données*

**À noter**
Pour utiliser Drush en local, il faut changer le nom du serveur par son ip.
En local, il faut remplacer `localhost` par `127.0.01`


### Schéma de la base de données

Les tables sont écrites par les modules activés par le profil d'installation standard.
On y retrouve les tables liés aux entités.
