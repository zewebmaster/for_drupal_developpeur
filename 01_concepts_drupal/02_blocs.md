### Le layout des blocs

Il est construit sur la base des régions qui sont déclarées dans votre thème.

```
// le fichier de configuration du thème
/core/themes/bartik/bartik.info.yml

```

Vous pouvez avoir un aperçu du rendu de votre thème en cliquant sur le lien d'aperçu.

**Attention, ce rendu n'est pas construit sur la base de la déclaration de votre thème, mais sur le template page.html.twig.**

Dans chaque région, vous choisissez le bloc que vous souhaitez afficher. Ces blocs peuvent être :

* Un bloc natif de l'installation Drupal ;
* Un bloc menu ;
* Un bloc vue ;
* Un bloc personnalisé ;
* Un bloc qui vous aurez construit dans un module ;


### Les blocs personnalisés

Comme vos types de contenus (node), vous avez la possibilité de créer vos propres types de blocs (c'est un type d'entités) en déclarant les champs qui les définissent.

Vous pouvez par la suite instancier autant de blocs que vous voulez - sur la base des modèles que vous avez défini - et les positionnez dans le layout précédent.  

### Configuration des blocs

À la déclaration d'un bloc, vous pouvez conditionnez son affichage à partir de différents critères :

* par page ;
* par type de contenu ;
* par rôle ;
