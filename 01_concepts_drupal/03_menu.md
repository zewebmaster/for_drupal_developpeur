### Déclaration des menus

La gestion des menus se fait en back-office. À l'installation, différents menus sont implémentés :

* menu d'administration ;
* menu principal ;
* menu footer ;
* menu utilisateur ;

Vous pouvez créer vos propres menus dans l'interface d'administration et y ajoutant les liens en remplissant le formulaire.

Vous ne pouvez lier que du contenu qui possède une route déclarée :

* Un contenu de votre site ;
* Une vue ;
* Un controller que vous aurez défini dans un module custom ;

Vous positionnez ensuite votre menu, comme un bloc, dans le layout de votre site.
Vous accédez de fait au même niveau de configuration : *par type de contenu, par page, par rôle...*
