### La structuration des données : les entités

Les entités sont la structure de base de Drupal : elles représentent un type de contenu (bundle), défini par un certain nombre de caractéristique (field).

Nativement, Drupal implémente différentes entités :

* Node ;
* Block ;
* Taxonomy ;
* Users ;

Vous pouvez en back office créer / paramétrer vos entités

* En leur ajoutant des valeurs ;
* En configurant le formulaire de contribution visant à les créer ;
* En leur définissant des modes d'affichages : full, teaser, custom ... ;


### Les fields

Ce sont tous les types de structure de données que l'on peut utiliser pour définir une entité.Ils représentent la manière dont sera stockée la données.

Un field posséde :

* Un libéllé personnalisable,
* Un nom machine, gravé dans le marbre ;
* Un type ;
* Une configuration pour la contribution et l'affichage.

--------------------------------------------------------------------------------

### Quelques entités spécifiques

--------------------------------------------------------------------------------

#### Les nodes

*gérés entre autre par le module node*

Par défaut, Drupal implémente page et articles.
Vous pouvez également créer vos propres structures de données pour les informations que vous avez à traiter : événement, offres promotionnelles, formations, objet quelconque ...

Ce sont les fields qui les définir.

**Bonne pratique :**
Ne pas multiplier les fields à l'infini : réutiliser les fields existants.
Ceci implique donc :

* Être attentif à son machine ;
* Être attentif à sa configuration ;

**Avec une limite :** si vous avez besoin de modifier la configuration d'un field existant pour une entité, il faut alors en créer un second (avec une nouvelle configuration), car la modification de configuration du field se propagerait à toutes les entités l'utilisant.

--------------------------------------------------------------------------------

#### La taxonomy

*gérée entre autre par le module taxonomy*

C'est un type d'entité particuliers qui permet de référencer vos contenus.

* Vocabulaires : Ce sont les grandes catégories de taxonomy ;
* Termes : Ce sont les éléments d'une catégorie de taxonomy ;

Un terme se définit nativement par :

* Un libellé ;
* Une description ;

Il peut être également enrichi par un ensemble de field (pour un vocabulaire donné).

Un terme de taxonomy possède déjà sa propre vue, agrégeant tous les contenus qui y font référence.

--------------------------------------------------------------------------------

#### Les blocks

*gérés entre autre par les modules block et block_content*

En back-office, dans le menu d'administration des blocs, il est possible de créer des types de blocs **de manière identique à la création de type de contenu** dont on pourra créer plusieurs instances.

Cette partie sera détaillée dans une partie dédiée à la gestion des blocs.

--------------------------------------------------------------------------------

#### Les utilisateurs

##### configuration générale

Nativement, un user est défini par un certain nombre de champs :

* username ;
* mail ;
* mot de passe ;
* rôle : les permissions qu'il aura pour interagir avec le système ;
* statut ;

*Il est possible de rajouter d'autres champs, tout comme n'importe quelle entité.*

La création de compte utilisateur peut se faire :

* Par l'administrateur seul ;
* Par l'utilisateur lui même, avec un système de vérification mail;

Le module user implémente un menu utilisateur spécifique, rendu dans un bloc, initialisé à l'installation de Drupal.

##### Rôle et permissions

Un rôle est un statut donné à l'utilisateur connecté, défini par un ensemble de permissions qui contrôle le niveau d'interaction avec le système.

* La création d'un rôle est une simple déclaration ;
* La configuration des permissions se fait par sélection, sur un ensemble de règles définies par les modules les utilisant.

*Dans un module custom, il est possible de créer ses propres permissions*

--------------------------------------------------------------------------------
