### Réalisation d'un site internet

#### Consigne

Construction un site qui manipule différents types de contenus, d'ordre éditorial :

* des pages institutionnelles ;
* des articles de blogs ;
* des éventements ;
* un formulaire de contact ;

On va construire également un type de contenu spécifique, propre à l'activité de l'association : une permanence, définie par les champs suivant :

* Utilisateur
* Date de début et de fin ;
* Un pôle d'activité parmi : accueil, vente, collecte, rangement
* Un commentaire ;

Le menu principal devra comporter :

* 2/3 liens vers des pages institutionnelles ;
* Un lien vers une liste de tous les articles ;
* Un lien vers le formulaire de contact ;

#### Guide

* Utiliser au moins un champs média pour vos pages ou vos articles ;
* Utiliser un champs adresse pour les événements ;
* Il existe un module natif Date range ... ;
* Créer pour les articles deux modes d'affichage : full, teaser ;
