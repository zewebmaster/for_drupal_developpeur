### Principe

Une vue est une agrégation de données rendu qui peut être rendu dans une page, un bloc, un flux.

La construction d'une vue se fait en plusieurs étapes :

* On agrège un ou plusieurs type.s d'entité ;
* On rend ces entités dans une liste, un tableau ... ;
* On configure le rendu  : un mode d'affichage défini **OU** des champs ;
* On affine les critères de la recherche (filtre, ordonnancement) ;

### Pour aller plus loin dans la configuration

Différentes options d'affichage :

* Le message en l'absence de résultat ;
* Des blocs d'information dans les zones *header* et *footer* ;
* Des tokens pour afficher des informations sur les données de la vue ;

La sélection des données :  

* Affichage de filtres de sélection pour l'utilisateur ;
* Agrégation des données ;
* Utilisation de filtres contextuels ;
* Configuration ajax ;
* Jointure ;

### Pour le rendu graphique

Il est possible de surcharger les templates utilisés par le module, en les redéclarant dans votre dossier de thème.

Liste des templates surchargeables :

```
// dans le dossier du module views
cd core/modules/views/templates

```

*On verra plus loin la notion de surcharge de thème*
