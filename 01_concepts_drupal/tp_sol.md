### Éléments de correction

#### La création des types de contenu / entités :

- [ ] Être attentif à la nomenclature des champs que l'on utilise ;
- [ ] Être attentif à la configuration du formulaire de contribution ;
- [ ] Être attentif à la configuration des modes d'affichages ;
- [ ] J'ai un patron d'alias ;

##### Configuration des visuels

- [ ] J'ai correctement défini la source des téléchargements ;
- [ ] J'ai correctement configurer les conditions d'upload ;
- [ ] J'ai un style d'image pour mon visuel ;
- [ ] J'ai configuré le widget de rendu dans les modes d'affichage de mes entités ;

##### Installation du module address

Source : [https://www.drupal.org/project/address](https://www.drupal.org/project/address)

```
cd custom_project
composer require drupal/address
drush en -y address
```

Ce module implémente un field préformatté address, accessible aux entités.
Il fonctionne entre autre avec d'autre modules, notamment drupal commerce, des modules de géolocalisation.
