### Documentation

Twig est un moteur de rendu php, issus de l'implémentation de Symfony.

[Documentation](http://twig.sensiolabs.org)
API : [https://symfony.com/doc/current/reference/twig_reference.html#functions](https://symfony.com/doc/current/reference/twig_reference.html#functions)

--------------------------------------------------------------------------------

### La syntaxe

Tous les fichiers de template doivent se terminer par l'extension `.html.twig`. 3 syntaxes à utiliser :

* {{ afficher }}
* {# commenter #}
* {% programmer %}

Exemple
```
<html>
  <head>
    <title>{{ head_title }}</title>
  </head>
  <body class="{{ classes }}" {{ attributes }}>
    <div class="row">
      {{ title }}
      {{ content.field_custom }} <!-- parcourir un tableau -->
    </div>
  </body>
</html>
```

--------------------------------------------------------------------------------

### Programmer

On peut utiliser des mécanismes de programmer de deux manières différentes :

#### Dans le template, 'à la volée' :

* {% if item ... %} {% else %} {% endif %}
* {% for item in items %} ({% else %}) {% endfor %}
* En utilisant des fonctions natives de symfoni : [https://symfony.com/doc/current/reference/twig_reference.html#functions](https://symfony.com/doc/current/reference/twig_reference.html#functions) ;
* En utilisant des fonctions fournies par Drupal : [https://www.drupal.org/node/2486991](https://www.drupal.org/node/2486991) ;

#### Avec des fonctions macro

On peut déclarer des fonctions dans le template.

Déclaration de la fonction
```
{% macro kamehameha(class, increment) %}
    <div class="{{ class }}">
      KAMEHAMEHA{% for i in range(0, increment) %}A{% endfor %}
    </div>
{% endmacro %}
```
Utilisation dans un template

```
<!-- pour importer la macro déclarée dans le template -->
{% import _self as function %}
<div class="dbz">
  {{ function.kamehameha('red', 10) }}
</div>
```

--------------------------------------------------------------------------------

### Utiliser les filtres

Vous pouvez utiliser un certain nombre de filtre pour formatter le rendu vos données.

`{{ title|upper }}`

Tous les filtres disponibles : [https://symfony.com/doc/current/reference/twig_reference.html#filters](https://symfony.com/doc/current/reference/twig_reference.html#filters)

**Important**
Drupal rend des fields imbriqués dans des div. Vous ne pouvez pas appliquer directement un filtre. Il vous faudra, soit parcourir la variable dans le template pour accéder à la valeur, soit isoler la valeur de la données dans une fonctions de preprocess.
