### Déclarer un thème

Seul fichier nécessaire : custom_theme.info.yml

```
core: 8.x
type: theme
base theme: false
name: 'Custom theme'
description: ''

regions:    // les régions de votre layout
libraries:  // vos librairies css et js
```

### Autres fichiers de configuration

Fichier | Fonction
------- | --------
montheme.theme | Les fonctions preprocess
montheme.librairies.yml | Librairies utilisées par votre thème
montheme.breakpoints.yml | Les points de rupture de votre thème


### Précisions sur le layout des régions

Vous définissez dans votre thème les régions que vous retrouverez sur la page d'administration du layout des blocks.

Cependant, l'aperçu qui vous est offert de votre layout repose sur le template de la page.

Si, dans ce template, vous n’appelez pas une région, vous ne la verrait pas apparaître dans l'aperçu.
