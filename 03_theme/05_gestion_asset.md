### Déclaration

On les précise dans le fichier de configuration `custom_theme.librairies.yml`. On y déclare :

* fichiers css,
* fichiers js,
* fichiers ressources externes

```
custom_theme.librairies.yml

front:
  css:
    base:
      'https://fonts.googleapis.com/icon?family=Material+Icons': { type: external }
      'https://fonts.googleapis.com/css?family=Oxygen': { type: external }

    theme:
      css/style.css: {}
  js:
    js/script.js: {}
  dependencies:
    - core/jquery
```

Si on veut utiliser globalement une librairie, on la déclare dans le fichier de configuration `custom_theme.info.yml`.

```
libraries:
  - 'custom_theme/front'
```
--------------------------------------------------------------------------------

### Utilisation

On peut utiliser ces librairie au globalement avec le thème, à la volée dans un fichier de preprocess, ou directement dans le thème.

Exemple d'implémentation :

* dans un preprocess
```
function custom_theme_preprocess_hook(&$variables) {
  $variables['#attached']['library'][] = 'custom_theme/custom_librairie';
}
```

* Dans le thème

`{{ attach_library('custom_theme/custom_librairie') }}`

* Dans un render array

`$build['#attached']['library'][] = 'custom_theme/custom_librairie';`
