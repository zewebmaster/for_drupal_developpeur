### Principe de base  

Drupal s'installe avec - pour simplifier - deux thèmes natifs :

* Bartik, pour le front ;
* Seven, pour le back ;


Les thèmes fonctionnent sur le même principe que les modules : ce sont des composants qui s'installe, s'active ou se déactive, se créer de toute pièce.

> Nous avons installé en début de formation le thème d'administration Adminimal.

Prenez le temps de regarder le thème bartik, en particuliers les fichiers `/core/themes/bartik/bartik.info.yml` et `/core/themes/bartik/bartik.module`.

**Bonne pratique** Un dossier contrib pour les thèmes communautaires et un dossier custom pour vos thèmes personnels.

--------------------------------------------------------------------------------

### Le rendu d'une page Drupal

Une page est composée par un ensemble de template, inclus les uns dans les autres.

Pour une page simple

```
html.html.twig
  page.html.twig
    region.html.twig
      block--menu.html.twig
        menu.html.twig
    region.html.twig
      block--maincontent.html.twig
        node.html.twig
          field.html.twig
    region.html.twig
      block--footer.html.twig
```

--------------------------------------------------------------------------------

### Commencer fonctionne le templating

Des fichiers de configuration .yml pour

* déclarer la structures du thèmes ;
* déclarer des librairies, des points de rupture ;
* déclarer des settings ;

Au niveau de la gestion des templates :

* Surcharge de template par simple déclaration : [https://www.drupal.org/node/2354645](https://www.drupal.org/node/2354645);
* Fonction de preprocess pour manipuler les données ;
* Hook disponible pour faire de la suggestion de template.  

Au niveau des librairies :

* Utilisation de librairies custom ;
* Possibilité de charger contextuellement des librairies ;
