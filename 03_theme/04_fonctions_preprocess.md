### Principe

Les fonctions de preprocess vous permettent d'intervenir sur les données avant que celle-ci ne soit rendues.

Dans ces fonctions, vous avez accès aux variables rendues et vous pouvez réaliser des tests sur des valeurs pour conditionner leur affichage, leur mise en forme, ajouter des structures (block, vue ...) de manière dynamique....

Ce un hook générique : `MODULE_NAME_preprocess_HOOK` ou `HOOK` définit la portion de thème rendue.

```
custom_theme_preprocess_node(&$variables) {}
custom_theme_preprocess_page(&$variables) {}
custom_theme_preprocess_block(&$variables) {}
custom_theme_preprocess_input(&$variables) {}

```
Ces fonctions prennent en paramètre &$variables qui autorise d’altérer les données qu'elles contiennent.

**À noter** Seul les données traitées par le hook sont disponible. *Un hook_preprocess_page embarque plus de données qu'un hook_preprocess_input*


### Implémentation

Dans le fichier `custom_theme.theme`.

Un exemple :

```
<?php

function custom_theme_preprocess_node(&$variables) {
  $node = $variables['node'];
  $type = $variables['node']->getType();
  $view_mode  = $variables['view_mode'];
}
```

--------------------------------------------------------------------------------

### Suggestion de thème

Avec le même principe de généricité, l'utilisation du hook `MODULE_NAME_theme_suggestions_HOOK_alter` vous permettra de définir des suggestions de template.

Un exemple d'implémentation pour avoir la possibilité d'utiliser des gabarits de page différents, fonction du type de node rendu.

```
<?php

function custom_theme_theme_suggestions_page_alter(array &$suggestions, array $variables) {

  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $content_type = $node->bundle();
    $suggestions[] = 'page__'.$content_type;
  }
}
```
