<?php

namespace Drupal\custom_module\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_module\Service\CustomService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomForm extends FormBase {

  /**
   * @var \Drupal\custom_module\Service\CustomService
   */
  protected $custom_service;

  public function __construct(CustomService $custom_service) {
    $this->custom_service = $custom_service;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('custom_module.custom_service'));
  }


  public function getFormId() {
   return 'custom_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => 'Formulaire de saisie d\'une permanance',
    ];

    // Date-time.
    $form['start'] = [
      '#type' => 'datetime',
      '#title' => 'Début de la permanance',
      '#date_increment' => 1,
      '#date_timezone' => drupal_get_user_timezone(),
      '#default_value' => drupal_get_user_timezone(),
    ];
    $form['stop'] = [
      '#type' => 'datetime',
      '#title' => 'Fin de la permanance',
      '#date_increment' => 1,
      '#date_timezone' => drupal_get_user_timezone(),
      '#default_value' => drupal_get_user_timezone(),
    ];

    // Select.
    $form['pole_select'] = [
      '#type' => 'select',
      '#title' => 'Choisir un pôle',
      '#options' => [
        'acc' => 'Accueil',
        'mag' => 'Magazin',
        'cai' => 'Caisse',
        'col' => 'Collecte',
      ],
      '#empty_option' => '-- Choisir dans les propostions codés en dur--',
    ];

    $form['pole'] = [
      '#type' => 'select',
      '#title' => 'Choisir un pôle',
      '#options' => $this->custom_service->getTermOptions('pole'),
      '#empty_option' => '-- Choisir dans les options du services--',
    ];

    // CheckBoxes.
    $form['post_comment'] = [
      '#type' => 'checkbox',
      '#title' => 'Laisser un message au coordinateur',
    ];

    // Textarea.
    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => 'Commentaire',
      '#description' => 'Laisser votre commentaire',
      '#states' => [
        'invisible' => [
          ':input[name="post_comment"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Valider',
    ];

    // Add a reset button that handles the submission of the form.
    $form['actions']['reset'] = [
      '#type' => 'button',
      '#button_type' => 'reset',
      '#value' => 'Réinitialiser',
      '#attributes' => [
        'onclick' => 'this.form.reset(); return false;',
      ],
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pole = $form_state->getValue('pole');
    if($pole == '') {
      $form_state->setErrorByName('pole', 'Choississez un pôle');
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $start = $form_state->getValue('start');
    $stop = $form_state->getValue('stop');
    $pole = $form_state->getValue('pole');

    $parameters = [
      'start' => $start,
      'stop' => $stop,
      'pole'=> $pole,
    ];

    $form_state->setRedirect('custom_module.redirection_form', $parameters);
  }
}
