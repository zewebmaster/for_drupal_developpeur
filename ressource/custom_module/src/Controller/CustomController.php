<?php

namespace Drupal\custom_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\taxonomy\Entity\Term;

class CustomController extends ControllerBase {

  public function render() {
    return [
      '#markup' => 'Hello World',
    ];
  }

  public function renderArgument($foo, $bar) {
    return [
      '#markup' => '<p>Ma page avec des argument $foo='.$foo.' $bar='.$bar.'</p>',
    ];
  }

  public function renderFormResult() {
    $params  = \Drupal::request()->query->all();

    $start =  array_key_exists('start', $params) ? $params['start'] : '';
    $stop  =  array_key_exists('stop', $params) ? $params['stop'] : '';
    $pole  =  array_key_exists('pole', $params) ? $params['pole'] : '';

    return [
      '#markup' => '<p>Ma page avec des argument $start='.$start.' $stop='.$stop.' $pole='.$pole.'</p>',
    ];
  }

  public function readDatabase() {

    $connection = \Drupal::database();

    $query = $connection->select('users', 'u');
    $result_users = $query->countQuery()->execute()->fetchField();

    $query  = \Drupal::entityQuery('node')
      ->condition('type', 'article', '');
    $nids = $query->execute();
    $result_article = sizeof($nids);

    // reqête qui aggrège les resultat
    $query  = \Drupal::entityQueryAggregate('node')
       ->condition('type', 'event', '')
       ->groupBy('field_pole')
       ->aggregate('nid', 'COUNT')
       ;
    $nids = $query->execute();
    $result = sizeof($nids);

    $list = [
      'Nombre utilisateur : ' . $result_users,
      'Nombre d\'article : ' . $result_article,
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $list,
    ];
  }
}
