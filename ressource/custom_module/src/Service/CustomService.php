<?php

namespace Drupal\custom_module\Service;

class CustomService {

  public function getTermOptions(string $vid){

    $options = [];
    $entity_type  = 'taxonomy_term';
    $terms = \Drupal::entityTypeManager()->getStorage($entity_type)->loadTree($vid);
    if(!empty($terms)) {
      foreach($terms as $term) {
        $term_name = $term->name;
        $options[$term->tid] = $term_name;
      }
    }
    return $options ;
  }

  public function do_bar(){
    return 'bar' ;
  }
}
