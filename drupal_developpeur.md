### Repo git

* [https://bitbucket.org/zewebmaster/for_drupal_developpeur/src/master/](https://bitbucket.org/zewebmaster/for_drupal_developpeur/src/master/)
* git clone https://zewebmaster@bitbucket.org/zewebmaster/for_drupal_developpeur.git

### Objectifs

* Appréhender l'environnement de développement Drupal ;
* Comprendre les concepts et la structuration des données ;
* Créer son propre module ;
* Modifier le comportement d'un module existant ;
* Aborder les notions relatives aux thèmes
* Concevoir un profil d'installation ;

### Prérequis

* Avoir des bases en POO ;
* Connaître php et avoir développé quelques script ;
* Notion de SQL, html/css, javascript... ;

--------------------------------------------------------------------------------

### Planning  

--------------------------------------------------------------------------------

#### Jour 1

Installation et prise en main de Drupal 8

* Présentation du framework ;
* Les outils utiles du développeur Drupal ;
* Installation de l'environnement de développement ;
* Le back-office et les concepts de Drupal ;
* Tp ;

#### Jour 2

Quelques notions d'architecture

* Architecture d'un module ;
* Architecture de Drupal ;

Intéragir avec Drupal

* Altérer le comportement d'un module existant : les hooks ;
* Tp ;

Différentes implémentations de composant

* Système de routage, de menu, de permissions ;
* Les controllers ;
* Les formulaires ;
* Tp ;
* Les Blocks ;
* Les injections de services ;
* Manipulation de la base de données ;
* Tp ;

#### Jour 3

Différentes implémentations de composant

* Les Blocks ;
* Les injections de services ;
* Manipulation de la base de données ;
* Tp ;

Découverte des notions liées aux thèmes

* Les bases d'un thème ;
* Concevoir un profil d'installation ;
* Découverte de quelques modules communautaires ;

#### Jour 4

* Gérer les configuration ;
* Mettre en place un workflow de développement ;
* Tp ;
* Les opérations de mise à jour ;
* Les profils d'installation ;
* Tp ;
