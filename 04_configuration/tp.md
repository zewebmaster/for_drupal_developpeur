#### Déployer votre projet sur un serveur de test

* Faite un premier déploiement de votre site ;
* Faite des modifications en local : création d'un type de node, création d'un block custom que vous aurez positionnez, implémenter un template ;
* Répercuter ces modifications dans votre serveur de tests.

#### Guide

* Utiliser au maximum la console... c'est plus rapide et cela va vite devenir un automatisme ;
* N'hésitez pas à vous appuyer sur les exemples de protocole fournis.
