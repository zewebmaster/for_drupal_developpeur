### Documentation

Configuration API [https://www.drupal.org/docs/8/api/configuration-api](https://www.drupal.org/docs/8/api/configuration-api)

Gérer ses configuration : [https://www.drupal.org/docs/8/configuration-management/managing-your-sites-configuration](https://www.drupal.org/docs/8/configuration-management/managing-your-sites-configuration)

### Principe de base

La configuration des entités, faite en back office peut être longue et fastidieuse et il n'est pas question de le refaire à la main pour chaque nouveau déploiement.

La configuration de votre système peut s'extraire sous la forme d'un ensemble de fichiers yml... et qui peut se réimporter dans un autre système. Vous pouvez ainsi synchroniser la configuration entre deux sites.

**Attention** La synchronisation de configuration ne peut se faire que sur des sites identiques, c'est à dire des sites clonés qui possède le même uuid.

Pour connaître l'uuid de son site : `drush cget system.site uuid`.

Cette information est stockée dans un fichier de configuration `system.site` et stocker en base de données, dans la table `config`.

*Vous ne pourrez pas gérer les configurations entre deux sites qui ont été installés séparément.*

### Configurer son export

La déclaration de la localisation du fichiers d'export se fait de le fichier `/sites/default/settings.php` grâce à la variable `$settings['config_sync_directory']`.

> Pour en savoir plus, prenez le temps de lire les commentaires dans le fichiers de settings !

Il est préférable de configurer le dossier de configuration, en dehors du webroot.

`$settings['config_sync_directory'] = '../config/sync';`

Ce dossier doit être présent dans votre git.

[https://www.drupal.org/docs/8/configuration-management/changing-the-storage-location-of-the-sync-directory](https://www.drupal.org/docs/8/configuration-management/changing-the-storage-location-of-the-sync-directory)

### Exporter sa configuration

Deux possibilités pour opérer :

* Vous pouvez le faire en back-office, dans le menu d'administration `/admin/config/development/configuration` --> vous télécharger vos sources ;
* Vous pouvez le faire avec une commande drush `drush cex -y` --> vos sources sont exportées dans le dossier déclaré en paramètre ;

**À noter** Gitter le dossier de synchronisation des configurations.
Il vous permettra d'historiser vos configurations.

> Du coup, soyez vigilent lorsque vous travaillez sur une configuration comme sur une entité. Penser à tous les aspects de l'entité (champs, mode d'affichage, formulaire contrib, image ...) pour ne pas avoir à y revenir.

### Importer sa configuration

De la même manière que pour l'exportation de la configuration, deux possibilité :

* Le faire en back office,
* Le faire avec un commande drush `drush cim -y`. - *Il faut que les sources sont dans le dossier de synchronisation du projet*

### Des outils complémentaires

drush cim tools:[https://www.drupal.org/project/drush_cmi_tools](https://www.drupal.org/project/drush_cmi_tools)
