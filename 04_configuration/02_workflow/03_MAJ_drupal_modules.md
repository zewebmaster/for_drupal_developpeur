### Mettre à jour Drupal

#### Avant de commencer

Les mises à jour de Drupal ou de l'un de ses modules est une opération à faire avec précaution.

> Dans la plupart des cas, cela se passe bien ... Mais il arrive que, ...

Commencer toujours sur votre serveur de test, en mettant les modules à jour un par un.

Penser à faire des vérifications sur les fonctionnalités traitées par le module avant de la valider.

Documentation Drupal : [https://www.drupal.org/docs/8/update/update-core-via-composer](https://www.drupal.org/docs/8/update/update-core-via-composer)

#### Le protocole

1. Faire un dump de sa base de données, avec vos outils de gestion de base de données ou avec Drush ;
2. Rechercher les modules à mettre à jour ;
3. Mettre à jour un par un les modules en vérifiant qu'il n'y ait pas une mise à jour de la base de données ;
4. Vider les caches ;
5. Gitter les fichiers composer.json et composer.lock ;

--------------------------------------------------------------------------------

#### Script

```
drush sql-dump > path/to/dump/dump_project_yymmdd.sql
composer outdated drupal/*
composer update drupal/module_name
drush updb
drush cr
git add composer.json
git add composer.lock
git commit -m "Update module_name"
```
