### Initialisation du projet

#### Configuration du serveur de test

* Configurer votre serveur de test :
  * Serveur virtuel ;
  * Base de données ;

#### Installer Drupal

* Installer Drupal 8 ;
* Installer les quelques modules dont vous êtes déjà sûr qu'ils vont vous servir : pathauto, metatag, sitemap, ...
* Lancer l'installeur ;
* Créer le dossier pour la synchronisation des configurations ;
* Exporter vos configurations ;

#### Initialiser le projet

* Vérifier la configuration de votre .gitignore ;
* Faites un dump de la base de données ;
* Initialiser votre repo local git ;
* Connecter le à votre remote ;
* Gitter et pousser votre projet ;

--------------------------------------------------------------------------------

### Script

Configuration apache
```
// conf vhost
<VirtualHost 127.0.0.110:80>
   DocumentRoot "/www/drupal_project/web"
   ServerName drupal.proj
</VirtualHost>

// conf hosts
127.0.0.110 drupal.proj
```

Création de la base de données
```
mysql -u root
// Créer votre bdd en sql
create database drupal_project;
```

Installation de drupal
```
cd www
composer create-project --prefer-dist drupal/recommended-project drupal_project
cd drupal_project
composer require drupal/module_name
composer require drupal/module_name
mkdir config && cd config && mkdir sync && cd ..
// après avoir lancer l'installeur
drush cex -y
```

Dump de la base
```
cd www/drupal_project
drush sql-dump > config/drupal_project_db_bare.sql
```

Workflow git
```
cd www/drupal_project
git init
git add origin remote add origin https://path/to/my/remote/drupal_project.git
git status
git add .
git commit -m "Init project"
git push --set-upstream origin master
```
