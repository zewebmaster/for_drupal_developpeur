#### Pendant le développement

* Développer vos fonctionnalités sur des branches, vous ne prendrez pas le risque de casser votre projet.
* Utiliser un kanban pour vos développements : cela vous permettra facilement d'isoler les tâches à réaliser, de repartir plus facilement les développements, de voir l'avancé du projet ;
* Adopter une nomenclature de commit : "Ticket#1234 - mon message" ;
* Faites des merges request et relisez vous les uns les autres ...
