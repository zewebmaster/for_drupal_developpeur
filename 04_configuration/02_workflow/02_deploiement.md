### Pour un premier déploiement

1. Cloner le projet depuis votre remote ;
2. Récupérer le dossier des files qui fait parti du .gitignore
3. Créer / vérifier les informations de `settings.php` : base de données, dossier de synchronisation, gestion du `local.settings.php` ;
4. Récupérer votre dossiers des files - *dossier non gitté*
5. Importer votre base de données ou installer la base de données bare.
6. Lancer le composer install ;
7. Vérifier les mises à jour de la base de données ;

### Pour appliquer des mises à jour

1. Faire un git pull ;
2. Si il y a des modifications sur le composer, faire un install ;
3. Si il y des modifications dans la configuration, les importer ;
4. Vérifier les mises à jour de la base de données ;

--------------------------------------------------------------------------------

### Script

Premier déploiement
```
cd www/
git clone https://path/to/my/remote/drupal_project.git project_stagin
cd web/sites/default
mv default.settings.php settings.php
// renseigner la base de données, le dossier de synchronisation
vim settings.php
cd ../..
composer install
drush sqlc < ../config/drupal_project_bare.sql
drush cr
```

Mise à jour
```
cd www/project_stagin
git pull
cd web
composer install
drush cim
drush updb
drush cr
```
