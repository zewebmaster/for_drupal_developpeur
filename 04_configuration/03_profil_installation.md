### Concevoir une distribution ?

C'est un profil d'installation Drupal qui installe et pré-configure une liste de modules. On obtient après l'installation de ce profil un site prêt à l'emploi.

Cela permet de créer des configurations de sites que l'on peut directement déployer sur différents serveurs.


### Comprendre les bases

Lorsque vous installez Drupal pour la première fois, vous devez choisir un profil d'installation :

* standard - *le profil que nous avons installé*
* minimum
* [Demo Umami](https://www.drupal.org/docs/8/umami-drupal-8-demonstration-installation-profile)

Dans le core, ces profils sont déclarés dans le dossier `/core/profiles`. Pour chaque profil, il y a un `profil.install` qui définit les tâches à réaliser à l'installation de Drupal.

### Déclarer un profil d'installation,

> Il faut se représenter la création d'un profil installation comme étant identique à la création d'un module.

Vous déclarez votre profile d'installation dans le dossier profile.

**Bonne pratique** Ranger ces profiles d'installation dans un dossier custom !
